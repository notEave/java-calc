package calcutils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

// split this.formattedCalculation into an array,
// on per-bracket basis, left-to-right, brackets
// that are indented inside each other calculated
// first.
// (3+2+(2/2))
// position0 : (2/2)
// position1 : (3+2)
// another array for solved brackets
// position0 + operand(+) + position1

public class Formula {
    public String formattedCalculation;
    public ArrayList<Integer> openingBrackets;
    public ArrayList<Integer> closingBrackets;
    // LinkedHashMap can be searched with index position
    // or key-value pairs. Here key is index position in
    // this.formattedCalculation. Value is type of operand.
    public LinkedHashMap<Integer, Character> operands;

    public Formula(String $baseCalculation) {
        // clear spaces and make text uppercase
        this.formattedCalculation = $baseCalculation.replaceAll(" ", "").toUpperCase();
        // find opening and closing brackets
        this.openingBrackets = FindSign(this.formattedCalculation, '(');
        this.closingBrackets = FindSign(this.formattedCalculation, ')');
        this.operands = FindOperands(this.formattedCalculation);
        SplitCalculation(this.formattedCalculation, this.openingBrackets, this.closingBrackets, this.operands);
    }

    private static ArrayList<Integer> FindSign(String $formattedCalculation, char $sign) {
        ArrayList<Integer> signArray = new ArrayList<Integer>();
        for ( int i = 0; i < $formattedCalculation.length(); i++ ) {
            if ( $formattedCalculation.charAt(i) == $sign ) {
                signArray.add(i);
            }
        }
        return signArray;
    }

    private static LinkedHashMap<Integer, Character> FindOperands(String $formattedCalculation) {
        LinkedHashMap<Integer, Character> operands = new LinkedHashMap<Integer, Character>();
        for ( int i = 0; i < $formattedCalculation.length(); i++ ) {
            switch ( $formattedCalculation.charAt(i) ) {
                case '+':
                    operands.put(i, '+');
                    break;
                case '-':
                    operands.put(i, '-');
                    break;
                case '*':
                    operands.put(i, '*');
                    break;
                case '/':
                    operands.put(i, '/');
                    break;
            }
        }
        return operands;
    }

    // TODO args to array
    private static void SplitCalculation(String $formattedCalculation, ArrayList<Integer> $openingBrackets, ArrayList<Integer> $closingBrackets, LinkedHashMap<Integer, Character> $operands) {
        System.out.println($openingBrackets + " " + $closingBrackets);
    }
}

/*
* FindOperands();
* ˇˇˇˇˇˇˇˇˇˇˇˇˇˇˇ
* jos avaavan sulun jälkeen tulee toinen avaava sulku,
* hyppää niin monen sulkevan sulun yli kuin ensimmäisen
* avaavan sulun jälkeen oli avaavia sulkuja
* jaa pilkotut laskut omiin muuttujiinsa, pilkottujen laskujen
* välissä olevat operandit tallennettava
* ((300-200)*(5+1))
* ^~         ~    ^
* (300-200)*(5+1)
* ^       ^
* *(5+1)
*
* avaavat sulut  [0, 1, 11]
* sulkevat sulut [9,15, 16]
*  0, 16
*  1,  9
* 11, 15
*
*
*/
